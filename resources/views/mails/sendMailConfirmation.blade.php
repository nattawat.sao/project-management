<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
    </head>
    <body>
        <center>
            <div class="space-3"/>
            <table class = "table_style" >
                <tr>
                    <td class="space width"></td>
                  </tr>
                <tr >
                    <td class ="left space red bold"><p class ="header">PROJECT MANAGEMENT</p><hr></td>
                </tr>
                <tr>
                    <td class ="left space"> Hi, <?php echo $details['username'];?> </br></td>
                </tr>
                <tr>
                    <td class ="left space">
                        </br>
                        <p>To complete email verification, Please press the button below.</p>
                        </br>
                    <td>
                </tr>
                <tr>
                    <td class ="left space"><a href="<?php echo 'http://127.0.0.1:8000/ConfirmedEmail/'.$details{'confirmMailToken'};?>"><button class="button uppercase"> verify </button></a> <td>
                </tr>
                <tr>
                    <td class="space-05"></td>
                </tr>
                <tr>
                    <td class ="right space"><p class ='font-size_12px'> send on <?php echo $details['updated_at']; ?></p></br>
                    <td>
                </tr>
            </table>
        </center>
    </body>
</html>
<style>
    body{
        background-color: #e0e0e0;
    }
    @font-face {
        font-family: 'Muli', sans-serif;
        src: "https://fonts.googleapis.com/css?family=Muli&display=swap";
    }

    * {
        font-family: 'Muli', sans-serif;
    }

    .font-size_12px{
        font-size: 12px;
    }
    .width{
        width:521px;
    }
    .button {
        background-color: #B82E35;
        border: 3px solid hsla(357, 60%, 45%, 0);;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        cursor : pointer;
        transition: 300ms ease all;
        width: 100%;
        outline: none;
    }
    .button:hover{
        color: #B82E35 !important;
        border: 3px solid #B82E35;
        background-color: #fff;
        transition: 300ms ease all;
        box-shadow: 5px 5px 10px rgba(68, 2, 2, 0.2);
        font-weight : bold;
    }

    .table_style{
        border: 0px; 
        background-color: #fff;
        text-align:center; 
        top:100px; 
        color:#000;
        box-shadow: 5px 5px 10px rgba(68, 2, 2, 0.2);
    }

    .center{
        text-align:center;
    }

    .left{
        text-align:left;
    }

    .right{
        text-align:right;
    }
    .space{
        padding: 10px 20px;
    }

    .space-05{
        padding: 50px;
    }

    .space-2{
        padding: 200px;
    }

    .space-3{
        padding: 100px;
    }

    .red{
        color:red;
    }

    .bold{
        font-weight : bold;
    }

    .uppercase{
        text-transform:uppercase;
    }

    .header{
        text-shadow: 5px 5px 10px rgba(68, 2, 2, 0.2);
    }
</style>