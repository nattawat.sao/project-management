/* eslint-disable handle-callback-err */
/* eslint-disable eol-last */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */

export function registerUser(credentials) {
    return new Promise((res, rej) => {
        this.axios.post('/api/signup', credentials)
            .then(response => {
                res(response.data)
            })

        .catch(err => {
            rej('An error occured.. try again later.')
        })
    })
}

export function signin(credentials) {
    return new Promise((res, rej) => {
        this.axios.post('/api/signin', credentials)
            .then(response => {
                res(response.data)
            })
            .catch(err => {
                rej('Wrong Email/Password combination.')
            })
    })
}

export function getLoggedinUser() {
    const userStr = localStorage.getItem('user')
    if (!userStr) {
        return null
    }

    return JSON.parse(userStr)
}