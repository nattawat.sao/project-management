/* eslint-disable eol-last */
/* eslint-disable indent */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
require('./bootstrap')

window.Vue = require('vue')

import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import Main from './App.vue'
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import storeData from './store.js'
import { routes } from './router.js'
import VueCookie from 'vue-cookie'

Vue.use(VueCookie)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(Vuelidate)
Vue.component('paginate', Paginate)
Vue.use(BootstrapVue)
Vue.component('paginate', VuejsPaginate)

Vue.config.productionTip = false
export const changeRoute = new Vue()

const router = new VueRouter({
    routes,
    mode: 'history'
})

const app = new Vue(Vue.util.extend({ router }, Main)).$mount('#app')