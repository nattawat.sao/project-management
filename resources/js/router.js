/* eslint-disable eol-last */
/* eslint-disable indent */

// public
import SignupComponent from './components/Public/Auth/Signup.vue'
import SigninComponent from './components/Public/Auth/Signin.vue'
import ForgotPass from './components/Public/Auth/ForgotPass.vue'
import EmailComfirmation from './components/Public/Auth/EmailCon.vue'
import Repassword from './components/Public/Auth/SendRepass.vue'
import RepasswordFromMail from './components/Public/Auth/ResetPasswordFromMail.vue'
// private
import ProjectListComponent from './components/Private/Admin/Project/AllProjectListComponent.vue'
import EmployeeListComponent from './components/Private/Admin/Auth/EmployeeListComponent.vue'
import CreateProComponent from './components/Private/Both/Auth/CreatePro.vue'
import ShowProject from './components/Private/Both/Project/ShowProject.vue'
import ProfileView from './components/Private/Both/Auth/ProfileView.vue'
import ProjectBlockList from './components/Private/Both/Project/ProjectBlockList.vue'
import ConfirmedEmail from './components/Public/Auth/ConfirmedEmail.vue'
export const routes = [{
        name: 'home',
        path: '/',
        component: ProjectListComponent,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'Employee  List',
        path: '/emp',
        component: EmployeeListComponent
    },
    {
        name: 'Signin',
        path: '/signin',
        component: SigninComponent
    },
    {
        name: 'Signup',
        path: '/signup',
        component: SignupComponent
    },
    {
        name: 'Forgot password',
        path: '/forgotPass',
        component: ForgotPass
    },
    {
        name: 'Create profile',
        path: '/createpro/:id',
        component: CreateProComponent
    },
    {
        name: 'Send require reset password',
        path: '/repass/:email',
        component: Repassword
    },
    {
        name: 'Email comfirmation',
        path: '/emailcon/:email',
        component: EmailComfirmation
    },
    {
        name: 'Reset Password form email',
        path: '/resetPassword-mail/:token',
        component: RepasswordFromMail
    },
    {
        name: 'Profile View',
        path: '/profile-view/:id',
        component: ProfileView
    },
    {
        name: 'Project View',
        path: '/project-view/:id',
        component: ShowProject
    },
    {
        name: 'Project Block List',
        path: '/ProjectBlockList',
        component: ProjectBlockList
    },
    {
        name: 'Success Confirmed Email',
        path: '/ConfirmedEmail/:token',
        component: ConfirmedEmail
    }
]