/* eslint-disable no-undef */
/* eslint-disable eol-last */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
import Vue from 'vue'
import Vuex from 'vuex'
import { getLoggedinUser } from './auth'

Vue.use(Vuex)

const user = getLoggedinUser()

export default new Vuex.Store({
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,
        reg_error: null,
        registeredUser: null
    },
    mutations: {
        signin(state) {
            state.loading = true
            state.auth_error = null
        },
        loginSuccess(state, payload) {
            state.auth_error = null
            state.isLoggedin = true
            state.loading = false
            state.currentUser = Object.assign({}, payload.user, { token: payload.token })
            localStorage.setItem('user', JSON.stringify(state.currentUser))
            console.log(currentUser)
        },
        logout(state) {
            localStorage.removeItem('user')
            state.isLoggedin = false
            state.currentUser = null
        }
    }
})