<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'taskTitle', 'taskDetail', 'start', 'end','softDelete','createBy','updateBy','user_id','project_id','responsible_person'
    ];

    public function userTask()
    {
        return $this->belongsTo('App\User');
    }

    public function projectTask()
    {
        return $this->belongsTo('App\Project');
    }
}