<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'role','username', 'email', 'password','createBy','updateBy', 'createById','resetPassword','confirmMailToken','emailConfirmation'
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function project()
    {
        return $this->hasMany('App\Project');
    }

    public function selfProject()
    {
        return $this->belongsToMany('App\Project','members')
                    ->withPivot([
                        'position',
                        'softDelete'
                    ])
                    ->wherePivot('softDelete', 'N');
    }

    protected $hidden = [
        'password', 'remember_token','resetPassword'
    ];
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}