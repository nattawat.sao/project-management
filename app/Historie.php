<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historie extends Model
{
    protected $fillable = [
        'user_id', 'icon','title','detail','created_at','updated_at','createBy','updateBy'
    ];
}
