<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'user_id', 'project_id','softDelete','createBy','updateBy','position','memberToken'
    ];

    public function getUser()
    {
        return $this->belongsTo('App\User')
                    ->where('softDelete', 'N');
    }
    public function getProject()
    {
        return $this->belongsTo('App\Project');
    }
}