<?php

namespace App\Http\Controllers;

use App\Task;
use App\Historie;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {   
        $task = Task::all()->where('project_id',$id)
                           ->where('softDelete','N');
        $getTask = null;
        foreach ($task as $tasks)
        {
            $getTask[] = $tasks;
        }
        // if ($getTask != null) {
            return response()->json($getTask);
        // }
        // else {
        //     return response()->json('Not found information');
        // }
    }

    public function selfTask($id)
    {   
        $task = Task::all()->where('project_id',$id)
                           ->where('softDelete','N')
                           ->where('responsible_person',$this->cur_id());
        $getTask = null;
        foreach ($task as $tasks)
        {
            $getTask[] = $tasks;
        }
        if ($getTask != null) {
            return response()->json($getTask);
        }
    }

    public function create(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all() , [
            'taskTitle' => 'required|string|max:255',
            'taskDetail' => 'required|string',
            'start' => 'required ',
            'end' => 'required '
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        $task = Task::create([
            'taskTitle' => $request->json()->get('taskTitle'),
            'taskDetail' => $request->json()->get('taskDetail'),
            'start' => $request->json()->get('start'),
            'end' => $request->json()->get('end'),
            'responsible_person' => $request->json()->get('responsible_person'),
            'createBy' => $this->cur_username(),
            'updateBy' => $this->cur_username(),
            'user_id' => $this->cur_id(),
            'project_id' => $id
        ]);
        #history funcrtion
        $getHistoryInfo = [
            'title' => $request['taskTitle'],
            'historyTitle' => 'Create New Task',
            'historyicon' => 'Create'
        ];
        $this->history($getHistoryInfo);
        return response()->json(compact('task'),201);
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $task = Task::find($id);
        $task->makeHidden([ 'created_at',
                            'softDelete','createBy','updateBy',
                            'project_id','user_id'])->toArray();
        return response()->json($task);
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $task->makeHidden([ 'id','created_at','updated_at',
                            'softDelete','createBy','updateBy',
                            'project_id','user_id'])->toArray();
        return response()->json($task);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all() , [
            'taskTitle' => 'required|string|max:255',
            'taskDetail' => 'required|string',
            'start' => 'required ',
            'end' => 'required '
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        Task::whereId($id)->update([
            'taskTitle' => $request->json()->get('taskTitle'),
            'taskDetail' => $request->json()->get('taskDetail'),
            'start' => $request->json()->get('start'),
            'end' => $request->json()->get('end'),
            'responsible_person' => $request->json()->get('responsible_person'),
            'updateBy' => $this->cur_username()
        ]);
        #history funcrtion
        $getHistoryInfo = [
            'title' => $request['taskTitle'],
            'historyTitle' => 'Update Task',
            'historyicon' => 'Update'
        ];
        $this->history($getHistoryInfo);
        $user = Task::find($id);
        return response()->json($user);
    }

    public function destroy($id)
    {
        $task['softDelete'] = 'Y';
        Task::whereId($id)->update($task);
        $getTask = Task::find($id);
        #history function
        $getHistoryInfo = [
            'title' => $getTask['taskTitle'],
            'historyTitle' => 'Destroy Task',
            'historyicon' => 'Destroy'
        ];
        $this->history($getHistoryInfo);
        return response()->json($getTask);
    }
}
