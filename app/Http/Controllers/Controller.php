<?php

namespace App\Http\Controllers;

use App\Historie;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function cur_role()
    {
        $current_user = auth()->user();
        $cur_role = $current_user['role'];

        return $cur_role;
    }

    public function cur_id(){
        $current_user = auth()->user();
        $cur_id = $current_user['id'];

        return $cur_id;
    }

    public function cur_username(){
        $current_user = auth()->user();
        $cur_username = $current_user['username'];

        return $cur_username;
    }

    public function cur_email(){
        $current_user = auth()->user();
        $cur_email = $current_user['email'];

        return $cur_email;
    }

    public function history($getHistoryInfo){

        $history = Historie::create([
            'title' => $getHistoryInfo['historyTitle'],
            'detail' => $getHistoryInfo['title'],
            'icon' => $getHistoryInfo['historyicon'],
            'createBy' => $this->cur_username(),
            'updateBy' => $this->cur_username(),
            'user_id' => $this->cur_id()
        ]);

        return $history;
    }
}
