<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
       //
    }

    public function create(Request $request, $id)
    { 
        $validator = Validator::make($request->json()->all() , [
            'firstName' => 'required|string|max:255',
            'surName' => 'string|max:255',
            'lastName' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'road' => 'required|string|max:255',
            'district' => 'required|string|max:255',
            'province' => 'required|string|max:255',
            'zip' => 'required|string|max:255',
            'phoneNumber' => 'required|string|max:255|unique:profiles'
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        $profile = Profile::create([
            'firstName' => $request->json()->get('firstName'),
            'surName' => $request->json()->get('surName'),
            'lastName' => $request->json()->get('lastName'),
            'address' => $request->json()->get('address'),
            'road' => $request->json()->get('road'),
            'district' => $request->json()->get('district'),
            'province' => $request->json()->get('province'),
            'zip' => $request->json()->get('zip'),
            'phoneNumber' => $request->json()->get('phoneNumber'),
            'user_id' => $this->cur_id(),
            'createBy' => $this->cur_username(),
            'updateBy' => $this->cur_username()
        ]);
        User::where('id',$id)->first()->update([
            'confirmMailToken' => null
        ]);
        //return to homepage
        return response()->json(compact('profile'),201);
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $profile = Profile::where('user_id',$id)->first();
        return response()->json($profile);
    }

    public function edit($id)
    {
        $profile = Profile::where('user_id',$id)->first();

        $profile->makeHidden([  'id','created_at','updated_at',
                                'createBy','updateBy'])->toArray();
        return response()->json($profile);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all() , [
            'firstName' => 'required|string|max:255',
            'surName' => 'max:255',
            'lastName' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'road' => 'required|string|max:255',
            'district' => 'required|string|max:255',
            'province' => 'required|string|max:255',
            'zip' => 'required|string|max:255',
            'phoneNumber' => 'required|string|max:255',
            'DoB' => 'required'
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        Profile::whereId($id)->update([
            'firstName' => $request->json()->get('firstName'),
            'surName' => $request->json()->get('surName'),
            'lastName' => $request->json()->get('lastName'),
            'address' => $request->json()->get('address'),
            'road' => $request->json()->get('road'),
            'district' => $request->json()->get('district'),
            'province' => $request->json()->get('province'),
            'zip' => $request->json()->get('zip'),
            'phoneNumber' => $request->json()->get('phoneNumber'),
            'updateBy' => $this->cur_username(),
            'DoB' => $request->json()->get('DoB')
        ]);
        
        $user = Profile::find($id);
        return response()->json($user);
    }

    public function destroy($id)
    {
        //
    }
}
