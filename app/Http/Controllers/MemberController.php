<?php

namespace App\Http\Controllers;

use App\Member;
use App\User;
use App\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendInvite;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function index()
    {
        //
    }
    public function create(Request $request,$id)
    {
        $getProject = Project::where('id',$id)->first(['id','projectTitle']);
        $getUser = User::where('email',$request['email'])->first();
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required|string|email|max:255',
            'position' => 'required|string|max:255'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        Member::create([
            'user_id' => $getUser['id'],
            'project_id' => $id,
            'position' => $request->json()->get('position'),
            'createBy' => $this->cur_username(),
            'updateBy' => $this->cur_username(),
        ]);
        $getUser['project'] = $getProject;
        $getUser['dateTime'] = date('Y-m-d H:i:s');
        $getUser['formUser'] = $this->cur_username();
        $getUser['position'] = $request['position'];
        $getUser->makeHidden([ 'id','emailConfirmation','created_at',
                                'updated_at','softDelete','createBy',
                                'updateBy','role','password','resetPassword',
                                'email','user_id'])->toArray();
        Mail::to($getUser['email'])->send(new sendInvite($getUser));
        return response()->json($getUser);
    }

    public function create_bu(Request $request,$id)
    {
        foreach ($request->json()->all() as $info) {
            $getProject = Project::where('id',$id)->first(['id','projectTitle']);
            $getUser = User::where('email',$info)->first();
            $validator = Validator::make($info, [
                'email' => 'required|string|email|max:255',
                'position' => 'required|string|max:255'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            Member::create([
                'user_id' => $getUser['id'],
                'project_id' => $id,
                'position' => $info['position'],
                'createBy' => $this->cur_username(),
                'updateBy' => $this->cur_username(),
            ]);
            $getUser['project'] = $getProject;
            $getUser['dateTime'] = date('Y-m-d H:i:s');
            $getUser['formUser'] = $this->cur_username();
            $getUser['position'] = $info['position'];
            $getUser->makeHidden([ 'id','emailConfirmation','created_at',
                                    'updated_at','softDelete','createBy',
                                    'updateBy','role','password','resetPassword',
                                    'email','user_id'])->toArray();
            Mail::to($getUser['email'])->send(new sendInvite($getUser));
        }
        return response()->json($getUser);
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return response()->json(Member::find($id)->makeHidden([ 'user_id', 'project_id',
                                                                'created_at','softDelete','updated_at',
                                                                'updateBy' ,'createBy'])
                                                 ->toArray());
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all(), [
            'position' => 'required|string|max:255'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        Member::find($id)->update([
            'position' => $request->json()->get('position'),
        ]);
        return response()->json('success');
    }

    public function destroy($id,Request $pro_id)
    {
        $member['softDelete'] = 'Y';
        Member::where('user_id', $id)->where('project_id', $pro_id['pro_id'])->update($member);
        $member = Member::find($id);
        return response()->json($member);
    }
}
