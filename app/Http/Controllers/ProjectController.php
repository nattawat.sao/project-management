<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    public function index()
    {
        if ($this->cur_role() == 'Admin') {
            
            $project = Project::all()->where('softDelete','N');
            $project->makeHidden(['user_id','projectDetail','created_at',
                                'updated_at','updateBy'])->toArray();
            foreach($project as $projects)
            {
                $getProject[] = $projects;
            }
            
            return response()->json($getProject);
        }
        else {
            return response()->json('Restricted to admin');
        }
    }

    public function userProjectList($id) // in project lisit
    {
        $getProject = User::find($id);
        foreach ($getProject->selfProject as $getSelfProject){
        }
        $getProject->makeHidden([   'emailConfirmation','created_at','updated_at',
                                    'createBy','updateBy',
                                    'role','user_id'])->toArray();
        return response()->json($getProject);
    }

    public function selfProjectList() // in project lisit
    {
        $getProject = User::find($this->cur_id());
        foreach ($getProject->selfProject as $getSelfProject){
        }
        $getProject->makeHidden([   'emailConfirmation','created_at','updated_at',
                                    'createBy','updateBy',
                                    'role','user_id'])->toArray();
        return response()->json($getProject);
    }

    public function create(Request $request)
    {
        //function for admin
        if ($this->cur_role() == 'Admin') {
            $validator = Validator::make($request->json()->all() , [
                'projectTitle' => 'required|string|max:255',
                'projectDetail' => 'required|string',
                'start' => 'required ',
                'end' => 'required '
            ]);
            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }
            $project = Project::create([
                'projectTitle' => $request->json()->get('projectTitle'),
                'projectDetail' => $request->json()->get('projectDetail'),
                'start' => $request->json()->get('start'),
                'end' => $request->json()->get('end'),
                'createBy' => $this->cur_username(),
                'updateBy' => $this->cur_username(),
                'user_id' => $this->cur_id(),
            ]);
            #history funcrtion
            $getHistoryInfo = [
                'title' => $request['projectTitle'],
                'historyTitle' => 'Create New Project',
                'historyicon' => 'Create'
            ];
            $this->history($getHistoryInfo);
            return response()->json(compact('project'),201);
        }
        //function for user
        else
        {
            return response()->json('Restricted to admin');
        }
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $getProject = Project::find($id);
        $getTask = Project::find($id)->getTask;
        $count = 0;
        foreach ($getTask as $getTaskProject){
            if ($getTaskProject['softDelete'] != 'Y') {
                $count++;
            }
        }
        $getProject['countTask'] = $count;
        return response()->json($getProject);
    }

    public function memberList($id)
    {
        $getMember = Project::find($id);
        foreach ($getMember->teamProject as $getTeamProject){
            $getTeamProject->makeHidden([   'emailConfirmation','created_at','updated_at',
                                            'createBy','updateBy',
                                            'role','user_id','email'])->toArray();
        }
        $getMember->makeHidden(['user_id','projectTitle','projectDetail',
                                'start','end','created_at','updated_at',
                                'softDelete','createBy','updateBy'])->toArray();
        return response()->json($getMember);
    }
    
    public function edit($id)
    {
        if ($this->cur_role() == 'Admin') 
        {
            $project = Project::find($id);

            $project->makeHidden([  'created_at','updated_at',
                                    'softDelete','createBy','updateBy'])->toArray();
            return response()->json($project);
        }
        else
        {
            return response()->json('Restricted to admin');
        }
    }

    public function update(Request $request, $id)
    {
        //function for admin
        if ($this->cur_role() == 'Admin') 
        {
            $validator = Validator::make($request->json()->all() , [
                'projectTitle' => 'required|string|max:255',
                'projectDetail' => 'required|string',
                'start' => 'required ',
                'end' => 'required '
            ]);
            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }
            Project::whereId($id)->update([
                'projectTitle' => $request->json()->get('projectTitle'),
                'projectDetail' => $request->json()->get('projectDetail'),
                'start' => $request->json()->get('start'),
                'end' => $request->json()->get('end'),
                'updateBy' => $this->cur_username()
            ]);
            #history funcrtion
            $getHistoryInfo = [
                'title' => $request['projectTitle'],
                'historyTitle' => 'Update Project',
                'historyicon' => 'Update'
            ];
            $this->history($getHistoryInfo);
            $user = Project::find($id);
            return response()->json($user);
        }
        // function for user
        else
        {
            return response()->json('Restricted to admin');
        }
    }

    public function destroy($id)
    {
        //function for admin
        if ($this->cur_role() == 'Admin') 
        {
            Project::whereId($id)->update([
                'softDelete' => 'Y'
            ]);
            $getProject = Project::find($id);
            #history funcrtion
            $getHistoryInfo = [
                'title' => $getProject['projectTitle'],
                'historyTitle' => 'Destroy Project',
                'historyicon' => 'Destroy'
            ];
            $this->history($getHistoryInfo);
            return response()->json($getProject);
        }
        //function for user
        else 
        {
            return response()->json('Restricted to admin');
        }
        
    }
}
