<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\Historie;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\Environment\Console;
//use JWTAuth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendResetPass;
use App\Mail\sendMailConfirmation;

class UserController extends Controller
{   
# Public function
    public function signup(Request $request)
    {
        $validator = Validator::make($request->json()->all() , [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'string|min:6|required_with:passwordConfirmation|same:passwordConfirmation', 
            'passwordConfirmation' => 'required|string|min:6'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create([
            'username' => $request->json()->get('username'),
            'email' => $request->json()->get('email'),
            'password' => Hash::make($request->json()->get('password')),
            'user_id' => $this->cur_id(),
            'confirmMailToken' => rand()
        ]);
        $token = JWTAuth::fromUser($user);
        $getUser = User::where('id',$user['id'])->first();
        $getUser->makeVisible('confirmMailToken')->toArray();
        Mail::to($getUser['email'])->send(new sendMailConfirmation($getUser)); #<-- somthing weong
        return response()->json(compact('user','token'),201);
    }

    public function signin(Request $request)
    {
        $getUser = User::where('email',$request['email'])->first();
        $getUser->makeVisible(['confirmMailToken','emailConfirmation'])->toArray();
        if ($getUser['softDelete'] == 'N') {
            // if ($getUser['emailConfirmation'] != 'Y') {
            //     return response()->json('Please confirm your email');
            // }
            $credentials = $request->json()->all();
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } 
            catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            // if ($getUser['confirmMailToken'] != null) {
            //     return '/createPro';
            // }
            return response()->json( compact('token','getUser'));
        }
        else {
            return response()->json('invalid_credentials');
        }
    }

    public function sendMailConfirmationAgain($email)
    {   
        $getUser = User::where('email',$email)->first();
        $getUser->makeHidden([ 'id','email', 'emailConfirmation',
                                'created_at','softDelete','createBy',
                                'updateBy' ,'role','user_id'])->toArray();
        $getUser->makeVisible('confirmMailToken')->toArray();
        Mail::to($getUser['email'])->send(new sendMailConfirmation($getUser));
        return response()->json($getUser);
    }

    public function sendMailConfirmationAgainInSignin(Request $request)
    {   
        $getUser = User::where('email',$request['email'])->first();
        $getUser->makeHidden([ 'id','email', 'emailConfirmation',
                                'created_at','softDelete','createBy',
                                'updateBy' ,'role','user_id'])->toArray();
        $getUser->makeVisible('confirmMailToken')->toArray();
        Mail::to($getUser['email'])->send(new sendMailConfirmation($getUser));
        return response()->json($getUser);
    }

    public function sendMailConfirmation($token)
    {   
        $getUser = User::where('confirmMailToken',$token)->first();
        $getUser->makeHidden([ 'id','email', 'emailConfirmation',
                                'created_at','softDelete','createBy',
                                'updateBy' ,'role','user_id'])->toArray();
        $getUser->makeVisible('confirmMailToken')->toArray();
        Mail::to($getUser['email'])->send(new sendMailConfirmation($getUser));
        return response()->json($getUser);
    }

    public function mailConfirmation($token)
    {
        $getInfo = User::where('confirmMailToken',$token)->first();
        if ($getInfo['emailConfirmation'] == 'Y') {
            return response()->json('Email was validate');
        }
        User::where('confirmMailToken',$token)->first()->update([
            'emailConfirmation' => 'Y'
        ]);
        $duser = User::where('confirmMailToken',$token)->first();
        return response()->json($duser);
    }

    public function sendReqRePass(Request $request)
    {
        $GetResetPassToken = rand();
        User::where('email', $request['email'])->first()->update([
            'resetPassword' => $GetResetPassToken,
            'updateBy' => 'System'
        ]);
        $getUser = User::where('email', $request['email'])->first();
        $getUser->makeHidden([ 'id','email', 'emailConfirmation',
                                'created_at','softDelete','createBy',
                                'updateBy' ,'role','user_id'])->toArray();
        $getUser->makeVisible('resetPassword')->toArray();
        Mail::to($getUser['email'])->send(new sendResetPass($getUser));
        return response()->json($getUser);
    }

    public function sendReqRePassAgain($email)
    {
        $GetResetPassToken = rand();
        User::where('email', $email)->first()->update([
            'resetPassword' => $GetResetPassToken,
            'updateBy' => 'System'
        ]);
        $getUser = User::where('email', $email)->first();
        $getUser->makeHidden([ 'id','email', 'emailConfirmation',
                                'created_at','softDelete','createBy',
                                'updateBy' ,'role','user_id'])->toArray();
        $getUser->makeVisible('resetPassword')->toArray();
        Mail::to($getUser['email'])->send(new sendResetPass($getUser));
        return response()->json($getUser);
    }

    public function getResetPassFromEmail($token)
    {
        $getInfor = User::where('resetPassword', $token)->first();
        if ($getInfor == null) {
            return response()->json('Token does not exist',404);
        }
        $getInfor->makeHidden([ 'emailConfirmation','created_at', 'user_id',
                                'updated_at','softDelete','createBy',
                                'updateBy','role' ,'resetPassword'])->toArray();
        return response()->json($getInfor);
        // return to reset password page
    }

    public function resetPassFromEmail(Request $request,$token)// get info form password page
    {
        $validator = Validator::make($request->json()->all() , [
            'password' => 'string|min:6|required_with:passwordConfirmation|same:passwordConfirmation',
            'passwordConfirmation' => 'required|string|min:6'
        ]);
        if (User::where('resetPassword', $token)->first() == null) {
            return response()->json('Token does not exist',404);
        }
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        User::where('resetPassword', $token)->first()->update([
            'resetPassword' => null,
            'updateBy' => 'System',
            'password' => Hash::make($request->json()->get('password')),
        ]);
        return response()->json('success',200);
    }
# Private function
    public function signout()
    {
        JWTAuth::invalidate();
        return response()->json([
        'statusCode' => 200,
        'statusMessage' => 'success',
        'message' => 'User Logged Out',
        ], 200);
    }

    public function changePassword(Request $request,$id){
        // if ($id == $this->cur_id()) {
            $validator = Validator::make($request->json()->all() , [
                'password' => 'string|min:6|required_with:passwordConfirmation|same:passwordConfirmation', 
                'passwordConfirmation' => 'required|string|min:6'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            User::find($id)->update([
                'resetPassword' => null,
                'updateBy' => 'System',
                'password' => Hash::make($request->json()->get('password')),
            ]);
            return response()->json('success',200);
        // }
        // return response()->json('Not allowed to change passwored');
    }

    public function index()
    {
        //function for admin
        // if($this->cur_role() == 'Admin'){
            $user = User::all()->where('softDelete','N');
            $user->makeHidden([ 'emailConfirmation','created_at','updated_at',
                                'createBy','updateBy','softDelete',
                                'user_id','profile'])->toArray();
            foreach($user as $users)
            {
                $users['profile'] = User::find($users['id'])->profile;
                $getUser[] = $users;
            }
            return response()->json($getUser);
        // }
        //function for user
        // else
        // {   
        //     return response()->json('Restricted to admin');
        // }
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function create(Request $request)
    {
        // function for admin
        // if($this->cur_role() == 'Admin')
        // {
            $validator = Validator::make($request->json()->all() , [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'string|min:6|required_with:passwordConfirmation|same:passwordConfirmation', 
                'passwordConfirmation' => 'required|string|min:6'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            $user = User::create([
                'username' => $request->json()->get('username'),
                'email' => $request->json()->get('email'),
                'password' => Hash::make($request->json()->get('password')),
                'user_id' => $this->cur_id(),
                'confirmMailToken' => rand()
            ]);
            $token = JWTAuth::fromUser($user);
            $getUser = User::where('id',$user['id'])->first();
            $getUser->makeVisible('confirmMailToken')->toArray();
            Mail::to($getUser['email'])->send(new sendMailConfirmation($getUser)); #<-- somthing weong
            #history funcrtion
            $getHistoryInfo = [
                'title' => $request['username'],
                'historyTitle' => 'Create New User',
                'historyicon' => 'Create'
            ];
            $this->history($getHistoryInfo);
            return response()->json(compact('user','token'),201);
        // }
        // //function for user
        // else
        // {
        //     return response()->json('Restricted to admin');
        // }
    }

    public function show($id)
    {
        $user = User::find($id);
        if ($user['softDelete'] == 'N') {
            return response()->json($user);
        }
        else {
            return response()->json('WARNING!! User id:'.$id.' not found.');
        }
    }

    public function edit($id)
    {
        //function for admin
        // if($this->cur_role() == 'Admin')
        // {
            $user = User::find($id);
            return response()->json($user);
        // }
        //function for user owner
        // elseif ($this->cur_id() == $id) {
        //     $user = User::find($id);
        //     $user->makeHidden([ 'id','emailConfirmation','created_at',
        //                         'updated_at','softDelete','createBy',
        //                         'updateBy','role','password','resetPassword'])->toArray();
        //     return response()->json($user);
        // }
        // //function for other user
        // else
        // {
        //     return response()->json('Restricted to admin and user owner');
        // }
    }

    public function update(Request $request, $id)
    {
        #function for owner user and admin
        $getUser = User::find($id);
        // if ($this->cur_role() == 'Admin' || $this->cur_id() == $id) {
            if ($request['email'] == $getUser['email']) {
                $validator = Validator::make($request->json()->all() , [
                    'username' => 'string|max:255',
                ]);
            }
            else {
                $validator = Validator::make($request->json()->all() , [
                    'username' => 'required|string|max:255',
                ]);
            }
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            User::whereId($id)->update([
                'username' => $request->json()->get('username'),
                'updateBy' => $this->cur_username()
            ]);
            #history funcrtion
            $getHistoryInfo = [
                'title' => $request['username'],
                'historyTitle' => 'Update User',
                'historyicon' => 'Update'
            ];
            $this->history($getHistoryInfo);
            $user = User::find($id);
            return response()->json($user);
        // }
        // //function for other user
        // else 
        // {
        //     return response()->json('Restricted to admin and user owner');
        // }
    }

    public function changeRole(Request $request, $id){
        User::whereId($id)->update([
            'role' => $request->json()->get('role')
        ]);
        //function for admin
        // if($this->cur_role() == "Admin")
        // {

            // $getUser = User::find($id);
            // if ($getUser['role'] == 'Admin') {
            //     $user['role'] = 'User';
            // }
            // else {
            //     $user['role'] = 'Admin';
            // }
            $user['updateBy'] = $this->cur_username();
            User::whereId($id)->update($user);
            #history funcrtion
            $user = User::find($id);
            $getHistoryInfo = [
                'title' => $user['username'],
                'historyTitle' => 'Change Role',
                'historyicon' => 'Update'
            ];
            $this->history($getHistoryInfo);
            return response()->json($user);
        // }
        //function for user
        // else
        // {
        //     return response()->json('Restricted to admin only');
        // }
    }

    public function destroy($id)
    {
        //function for admin
        // if($this->cur_role() == "Admin")
        // {
            User::whereId($id)->update([
                'softDelete' => 'Y',
                'updateBy' => $this->cur_username()
            ]);
            #history function
            $getUser = User::find($id);
            $getHistoryInfo = [
                'title' => $getUser['username'],
                'historyTitle' => 'Destroy User',
                'historyicon' => 'Destroy'
            ];
            $this->history($getHistoryInfo);
            return response()->json($getUser);
        // }
        //function for user
        // else
        // {
        //     return response()->json('Restricted to admin only');
        // }
    }
}