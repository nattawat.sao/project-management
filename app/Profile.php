<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'firstName',
        'surName',
        'lastName',
        'address',
        'road',
        'district',
        'province',
        'zip',
        'phoneNumber',
        'createBy',
        'updateBy',
        'user_id',
        'DoB'
    ];
}
