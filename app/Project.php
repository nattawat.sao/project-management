<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'projectTitle', 'projectDetail', 'start', 'end','softDelete','createBy', 'updateBy', 'user_id'
    ];

    public function getTask()
    {
        return $this->hasMany('App\Task');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function teamProject()
    {
        return $this->belongsToMany('App\User','members')
                    ->withPivot([
                        'id',
                        'position',
                        'softDelete',
                        'updated_at'
                    ])
                    ->wherePivot('softDelete', 'N');
    }
}
