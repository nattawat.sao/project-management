<?php


Route::get('/signup', function () {return view('post');})->name('signup');
Route::get('/signin', function () {return view('post');})->name('login');
Route::get('/forgotpass', function () {return view('post');})->name('forgotpass');
  Route::get('/', function () {return view('post');})->name('home');
  Route::get('/project-view/{id}', function () {return view('post');})->name('projectview');
  Route::get('/{any}', function () {
    return view('post');
  })->where('any', '.*');
Route::group(['middleware' => 'auth'],function(){

});

// //Route for normal user
// Route::group(['middleware' => ['auth']], function () {
//   Route::get('/', function () {return view('post');})->name('home');
// });
// //Route for admin
// Route::group(['prefix' => 'admin'], function(){
//   Route::group(['middleware' => ['admin']], function(){
//     Route::get('/', function () {return view('post');})->name('home');
//   });
// });