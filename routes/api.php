<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;

Route::post('signup', 'UserController@signup');
Route::post('signin', 'UserController@signin');
Route::post('sendReqRePass', 'UserController@sendReqRePass');
Route::post('getResetPassFromEmail/{token}', 'UserController@getResetPassFromEmail')->where('token', '.*');
Route::put('resetPassFromEmail/{token}', 'UserController@resetPassFromEmail')->where('token', '.*');
Route::post('sendMailConfirmation/{token}', 'UserController@sendMailConfirmation')->where('token', '.*');
Route::put('mailConfirmaion/{token}', 'UserController@mailConfirmation')->where('token', '.*');
Route::post('sendMailConfirmationAgain/{email}', 'UserController@sendMailConfirmationAgain')->where('email', '.*');
Route::post('sendMailConfirmationAgainInSignin', 'UserController@sendMailConfirmationAgainInSignin');
Route::post('sendReqRePassAgain/{email}', 'UserController@sendReqRePassAgain')->where('email', '.*');

Route::group(['middleware' => ['jwt.verify']], function() 
{
    //Authenticate
    Route::get('signout', 'UserController@signout');
    Route::post('createProfile/{id}','ProfileController@create');
    Route::get('showSelfUser', 'UserController@getAuthenticatedUser');
    Route::put('changePassword/{id}','UserController@changePassword');
    
    // User api
    Route::get('userList', 'UserController@index');
    Route::get('showUser/{id}', 'UserController@show');
    Route::get('editUser/{id}','UserController@edit');
    Route::put('updateUser/{id}','UserController@update');
    Route::put('destroyUser/{id}', 'UserController@destroy');
    Route::post('createUser', 'UserController@create');
    Route::put('changeRole/{id}', 'UserController@changeRole');
    
    // Member api
    Route::post('createMember/{id}','MemberController@create');
    Route::put('destroyMember/{id}','MemberController@destroy');

    // Project api
    Route::get('showUserProject/{id}','ProjectController@userProjectList');
    Route::get('projectList','ProjectController@index');
    Route::get('selfProjectList','ProjectController@selfProjectList');
    Route::post('createProject','ProjectController@create');
    Route::get('showProject/{id}','ProjectController@show');
    Route::get('memberListInProject/{id}','ProjectController@memberList');
    Route::get('editProject/{id}','ProjectController@edit');
    Route::put('updateProject/{id}','ProjectController@update');
    Route::put('destroyProject/{id}','ProjectController@destroy');

    // Task api
    #get project id
        Route::get('TaskList/{id}','TaskController@index');
        Route::get('selfTaskList/{id}','TaskController@selfTask');
        Route::post('createTask/{id}','TaskController@create');
    #get task id
        Route::get('showTask/{id}','TaskController@show');
        Route::get('editTask/{id}','TaskController@edit'); 
        Route::put('updateTask/{id}','TaskController@update'); 
        Route::put('destroyTask/{id}','TaskController@destroy');

    //Profile api
    Route::get('showProfile/{id}','ProfileController@show');
    Route::get('editProfile/{id}','ProfileController@edit');
    Route::put('updateProfile/{id}','ProfileController@update');
    #member
        Route::post('inviteMember/{id}','MemberController@create'); // id = project_id
        Route::get('editMember/{id}','MemberController@edit');
        Route::put('updateMember/{id}','MemberController@update');
        Route::put('leaveMember/{id}','MemberController@destroy');
    //History api
        #get user id
        Route::get('showHistory/{id}','HistorieController@show');
});